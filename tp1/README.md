# TP reseau 1

## I. Exploration locale en solo

### 1. Affichage d'informations sur la pile TCP/IP locale

🌞 Affichez les infos des cartes réseau de votre PC

Carte réseau sans fil Wi-Fi :

    PS C:\Users\yanic> ipconfig all 

    Description. . . . . . . . . . . . . . : Intel(R) Wi-Fi 6E AX211 160MHz

    Adresse physique . . . . . . . . . . . : E4-0D-36-06-76-60

    Adresse IPv4. . . . . . . . . . . . . .: 10.33.48.40(préféré)

Carte Ethernet Ethernet :

    PS C:\Users\yanic> ipconfig all

    Description. . . . . . . . . . . . . . : Realtek PCIe GbE Family Controller
    Adresse physique . . . . . . . . . . . : BC-EC-A0-11-AD-7B

🌞 Affichez votre gateway

    PS C:\Users\yanic> ipconfig all

    Passerelle par défaut. . . . . . . . . : 10.33.51.254

🌞 Déterminer la MAC de la passerelle

    PS C:\Users\yanic> ipconfig all

    Adresse physique . . . . . . . . . . . : BC-EC-A0-11-AD-7B

🌞 Trouvez comment afficher les informations sur une carte IP (change selon l'OS)

    Adresse IPv4 : 10.33.48.40
    Adresse physique : E4-0D-36-06-76-60
    Passerelle : 10.33.51.254

### 2. Modifications des informations

#### A. Modification d'adresse IP (part 1)

🌞 Utilisez l'interface graphique de votre OS pour changer d'adresse IP 

    Adresse IPv4. . . . . . . . . . . . . .: 10.33.48.50

🌞 Il est possible que vous perdiez l'accès internet.

## II. Exploration locale en duo

🌞 Modifiez l'IP des deux machines pour qu'elles soient dans le même réseau

    PS C:\Users\yanic> ipconfig

    IP : 10.10.10.99
    Masque : 255.255.255.0

🌞 Vérifier à l'aide d'une commande que votre IP a bien été changée

    PS C:\Users\yanic> ipconfig

    Adresse IPv4. . . . . . . . . . . . . .: 10.10.10.11
    Masque de sous-réseau. . . . . . . . . : 255.255.255.0

🌞 Vérifier que les deux machines se joignent

    PS C:\Users\yanic> ping 10.10.10.99

    Envoi d’une requête 'Ping'  10.10.10.99 avec 32 octets de données :
    Réponse de 10.10.10.99 : octets=32 temps=5 ms TTL=128
    Réponse de 10.10.10.99 : octets=32 temps=6 ms TTL=128
    Réponse de 10.10.10.99 : octets=32 temps=5 ms TTL=128
    Réponse de 10.10.10.99 : octets=32 temps=6 ms TTL=128

🌞 Déterminer l'adresse MAC de votre correspondant

    10.10.10.99           bc-0f-f3-d6-21-ff     dynamique

### 4. Petit chat privé

🌞 sur le PC client

    PS C:\Users\yanic\Desktop\Yanice_ynov\Cour Réseau> .\nc.exe 10.10.10.99 8888

🌞 Visualiser la connexion en cours

    PS C:\Users\yanic> netstat -a -n -b | Select-String "8888"

    TCP    10.10.10.12:50357      10.10.10.99:8888       ESTABLISHED

🌞 Pour aller un peu plus loin

    PS C:\Users\yanic> netstat -a -n -b | select-string 8888

    TCP    10.10.10.12:51946      10.10.10.99:8888       ESTABLISHED


    PS C:\Users\yanic> netstat -a -n -b | select-string 8888 -context 0,1

    >   TCP    10.10.10.12:51946      10.10.10.99:8888       ESTABLISHED
    [nc.exe]

### 5. Firewall

🌞 Activez et configurez votre firewall

Création d'une règle qui permet de recevoir et envoyer des pings

    PS C:\Users\yanic> ping 10.10.10.99

    Envoi d’une requête 'Ping'  10.10.10.99 avec 32 octets de données :
    Réponse de 10.10.10.99 : octets=32 temps=5 ms TTL=128
    Réponse de 10.10.10.99 : octets=32 temps=4 ms TTL=128
    Réponse de 10.10.10.99 : octets=32 temps=6 ms TTL=128
    Réponse de 10.10.10.99 : octets=32 temps=3 ms TTL=128

    Statistiques Ping pour 10.10.10.99:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
    Durée approximative des boucles en millisecondes :
    Minimum = 3ms, Maximum = 6ms, Moyenne = 4ms

### 6. Utilisation d'un des deux comme gateway

🌞Tester l'accès internet

    PS C:\Users\yanic> ipconfig

    Configuration IP de Windows


    Carte Ethernet Ethernet :

    Suffixe DNS propre à la connexion. . . :
    Adresse IPv6 de liaison locale. . . . .: fe80::10c0:b6a2:d056:93f7%12
    Adresse IPv4. . . . . . . . . . . . . .: 192.168.137.12
    Masque de sous-réseau. . . . . . . . . : 255.255.255.0
    Passerelle par défaut. . . . . . . . . : 192.168.137.1
#
    C:\Users\yanic>ping 1.1.1.1

    Envoi d’une requête 'Ping'  1.1.1.1 avec 32 octets de données :
    Réponse de 1.1.1.1 : octets=32 temps=16 ms TTL=56
    Réponse de 1.1.1.1 : octets=32 temps=27 ms TTL=56
    Réponse de 1.1.1.1 : octets=32 temps=22 ms TTL=56
    Réponse de 1.1.1.1 : octets=32 temps=17 ms TTL=56

    Statistiques Ping pour 1.1.1.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
    Durée approximative des boucles en millisecondes :
    Minimum = 16ms, Maximum = 27ms, Moyenne = 20ms

🌞 Prouver que la connexion Internet passe bien par l'autre PC

    PS C:\Users\yanic> tracert 192.168.137.1

    Détermination de l’itinéraire vers THOMAS [192.168.137.1]
    avec un maximum de 30 sauts :

    1    10 ms     3 ms     3 ms  THOMAS [192.168.137.1]

    Itinéraire déterminé.

## III. Manipulations d'autres outils/protocoles côté client

### 1. DHCP

🌞Exploration du DHCP, depuis votre PC

    PS C:\Users\yanic> ipconfig /all

    Configuration IP de Windows

    Carte réseau sans fil Wi-Fi :
    Suffixe DNS propre à la connexion. . . :   Description. . . . . . . . . . . . . . : Intel(R) Wi-Fi 6E AX211 160MHz
    Adresse physique . . . . . . . . . . . : E4-0D-36-06-76-60
    DHCP activé. . . . . . . . . . . . . . : Oui
    Configuration automatique activée. . . : Oui
    Adresse IPv6 de liaison locale. . . . .: fe80::d807:ee16:b687:3d08%10(préféré)
    Adresse IPv4. . . . . . . . . . . . . .: 10.33.48.40(préféré)
    Masque de sous-réseau. . . . . . . . . : 255.255.252.0
    Bail obtenu. . . . . . . . . . . . . . : lundi 16 octobre 2023 13:53:34
    Bail expirant. . . . . . . . . . . . . : mardi 17 octobre 2023 13:53:28
    Passerelle par défaut. . . . . . . . . : 10.33.51.254
    Serveur DHCP . . . . . . . . . . . . . : 10.33.51.254
    IAID DHCPv6 . . . . . . . . . . . : 132386102
    DUID de client DHCPv6. . . . . . . . : 00-01-00-01-2C-82-50-96-BC-EC-A0-11-AD-7B
    Serveurs DNS. . .  . . . . . . . . . . : 10.33.10.2
                                       8.8.8.8
    NetBIOS sur Tcpip. . . . . . . . . . . : Activé


### 2. DNS

🌞** Trouver l'adresse IP du serveur DNS que connaît votre ordinateur**

    PS C:\Users\yanic> ipconfig /all

    Serveurs DNS. . .  . . . . . . . . . . : 10.33.10.2

🌞 Utiliser, en ligne de commande l'outil nslookup (Windows, MacOS) ou dig (GNU/Linux, MacOS) pour faire des requêtes DNS à la main

    PS C:\Users\yanic> nslookup google.com 8.8.8.8
    Serveur :   dns.google
    Address:  8.8.8.8

    Réponse ne faisant pas autorité :
    Nom :    google.com
    Addresses:  2a00:1450:4007:818::200e
          142.250.179.110

    PS C:\Users\yanic> nslookup ynov.com 8.8.8.8
    Serveur :   dns.google
    Address:  8.8.8.8

    Réponse ne faisant pas autorité :
    Nom :    ynov.com
    Addresses:  2606:4700:20::ac43:4ae2
          2606:4700:20::681a:ae9
          2606:4700:20::681a:be9
          104.26.10.233
          104.26.11.233
          172.67.74.226
#
    PS C:\Users\yanic> nslookup 231.34.113.12 8.8.8.8
    Serveur :   dns.google
    Address:  8.8.8.8

    *** dns.google ne parvient pas à trouver 231.34.113.12 : Non-existent domain
    PS C:\Users\yanic> nslookup 78.34.2.17 8.8.8.8
    Serveur :   dns.google
    Address:  8.8.8.8

    Nom :    cable-78-34-2-17.nc.de
    Address:  78.34.2.17

## IV. Wireshark

🌞 Utilisez le pour observer les trames qui circulent entre vos deux carte Ethernet. Mettez en évidence :