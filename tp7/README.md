# TP7 : Do u secure

## 0. Setup

## 1. Fingerprint

### 🌞 Effectuez une connexion SSH en vérifiant le fingerprint

    PS C:\Windows\system32> ssh yanice@10.7.1.11
    The authenticity of host '10.7.1.11 (10.7.1.11)' can't be established.
    ED25519 key fingerprint is SHA256:/gMjyJCttiXiYJ5pHIllXeXAOGrJ9HtzccqLH6zUm1M.
    This host key is known by the following other names/addresses:
    C:\Users\yanic/.ssh/known_hosts:1: 10.3.1.12
    C:\Users\yanic/.ssh/known_hosts:4: 10.3.1.11
    C:\Users\yanic/.ssh/known_hosts:5: 10.3.2.12
    C:\Users\yanic/.ssh/known_hosts:6: 10.3.1.254
    C:\Users\yanic/.ssh/known_hosts:7: 10.4.1.254
    C:\Users\yanic/.ssh/known_hosts:8: 10.4.1.253
    C:\Users\yanic/.ssh/known_hosts:9: 10.4.1.12
    C:\Users\yanic/.ssh/known_hosts:10: 10.4.1.200
    (8 additional names omitted)
    Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
    Warning: Permanently added '10.7.1.11' (ED25519) to the list of known hosts.
    yanice@10.7.1.11's password:
    Last login: Fri Nov 24 14:40:02 2023 from 10.7.1.1
#
    [yanice@john ~]$ sudo ssh-keygen -l -f /etc/ssh/ssh_host_ed25519_key
    [sudo] password for yanice:
    256 SHA256:/gMjyJCttiXiYJ5pHIllXeXAOGrJ9HtzccqLH6zUm1M /etc/ssh/ssh_host_ed25519_key.pub (ED25519)

## 2. Conf serveur SSH

### 🌞 Consulter l'état actuel

    [yanice@routeur ~]$ ss -lntp | grep "22"
    LISTEN 0      128          0.0.0.0:22        0.0.0.0:*
    LISTEN 0      128             [::]:22           [::]:*

### 🌞 Modifier la configuration du serveur SSH

    [yanice@routeur ~]$ ss -tln | grep 22
    LISTEN 0      128       10.7.1.254:22        0.0.0.0:*


