# TP reseau 2

## I. Setup IP

🌞 Mettez en place une configuration réseau fonctionnelle entre les deux machines

    PS C:\Windows\system32> New-NetIPAddress -InterfaceIndex 11 -IPAddress 192.168.1.2 -PrefixLength 30


    IPAddress         : 192.168.1.2 🌞
    InterfaceIndex    : 11
    InterfaceAlias    : Ethernet
    AddressFamily     : IPv4
    Type              : Unicast
    PrefixLength      : 30 🌞
    PrefixOrigin      : Manual
    SuffixOrigin      : Manual
    AddressState      : Tentative
    ValidLifetime     :
    PreferredLifetime :
    SkipAsSource      : False
    PolicyStore       : ActiveStore

    Adresse réseau:
     - 192.168.1.0

    Adresse Broadcast:
     - 192.168.1.3

🌞 Prouvez que la connexion est fonctionnelle entre les deux machines

    PS C:\Windows\system32> ping 192.168.1.1

    Envoi d’une requête 'Ping'  192.168.1.1 avec 32 octets de données :
    Réponse de 192.168.1.1 : octets=32 temps=1 ms TTL=64
    Réponse de 192.168.1.1 : octets=32 temps=4 ms TTL=64
    Réponse de 192.168.1.1 : octets=32 temps=1 ms TTL=64
    Réponse de 192.168.1.1 : octets=32 temps=4 ms TTL=64

    Statistiques Ping pour 192.168.1.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
    Durée approximative des boucles en millisecondes :
    Minimum = 1ms, Maximum = 4ms, Moyenne = 2ms

🌞 Wireshark it

## II. ARP my bro

🌞 Check the ARP table

    PS C:\Windows\system32> arp /a

    Interface : 169.254.148.110 --- 0x7
    Adresse Internet      Adresse physique      Type
    169.254.255.255       ff-ff-ff-ff-ff-ff     statique
    224.0.0.22            01-00-5e-00-00-16     statique
    224.0.0.251           01-00-5e-00-00-fb     statique
    224.0.0.252           01-00-5e-00-00-fc     statique
    239.255.255.250       01-00-5e-7f-ff-fa     statique
    255.255.255.255       ff-ff-ff-ff-ff-ff     statique

    Interface : 10.33.72.140 --- 0x9
    Adresse Internet      Adresse physique      Type
    10.33.65.136          74-3a-f4-ce-4d-61     dynamique
    10.33.65.238          b0-7d-64-ae-74-f5     dynamique
    10.33.66.138          14-5a-fc-79-6c-cb     dynamique
    10.33.68.252          94-e2-3c-43-51-98     dynamique
    10.33.69.103          1c-bf-c0-6d-5d-09     dynamique
    10.33.70.241          4c-44-5b-41-6b-b7     dynamique
    10.33.71.24           cc-d9-ac-c4-17-4c     dynamique
    10.33.71.244          10-68-38-f9-61-f8     dynamique
    10.33.73.197          34-6f-24-c4-e2-4b     dynamique
    10.33.74.50           90-cc-df-5e-f3-0d     dynamique
    10.33.75.39           70-d8-23-1d-5b-a5     dynamique
    10.33.76.133          3c-9c-0f-c8-77-ec     dynamique
    10.33.76.233          f8-b5-4d-43-18-4a     dynamique
    10.33.76.234          e8-fb-1c-72-06-33     dynamique
    10.33.79.254          7c-5a-1c-d3-d8-76     dynamique 🌞
    10.33.79.255          ff-ff-ff-ff-ff-ff     statique
    224.0.0.22            01-00-5e-00-00-16     statique
    224.0.0.251           01-00-5e-00-00-fb     statique
    224.0.0.252           01-00-5e-00-00-fc     statique
    239.255.255.250       01-00-5e-7f-ff-fa     statique
    255.255.255.255       ff-ff-ff-ff-ff-ff     statique

    Interface : 192.168.1.2 --- 0xb
    Adresse Internet      Adresse physique      Type
    192.168.1.1           00-d8-61-8a-6e-78     dynamique 🌞
    192.168.1.3           ff-ff-ff-ff-ff-ff     statique
    224.0.0.22            01-00-5e-00-00-16     statique
    224.0.0.251           01-00-5e-00-00-fb     statique
    239.255.255.250       01-00-5e-7f-ff-fa     statique

    Interface : 169.254.87.5 --- 0x12
    Adresse Internet      Adresse physique      Type
    169.254.255.255       ff-ff-ff-ff-ff-ff     statique
    192.168.139.254       00-50-56-ec-20-cd     dynamique
    224.0.0.22            01-00-5e-00-00-16     statique
    224.0.0.251           01-00-5e-00-00-fb     statique
    224.0.0.252           01-00-5e-00-00-fc     statique
    239.255.255.250       01-00-5e-7f-ff-fa     statique
    255.255.255.255       ff-ff-ff-ff-ff-ff     statique

    Interface : 169.254.236.202 --- 0x15
    Adresse Internet      Adresse physique      Type
    169.254.255.255       ff-ff-ff-ff-ff-ff     statique
    192.168.46.254        00-50-56-e2-52-49     dynamique
    224.0.0.22            01-00-5e-00-00-16     statique
    224.0.0.251           01-00-5e-00-00-fb     statique
    224.0.0.252           01-00-5e-00-00-fc     statique
    239.255.255.250       01-00-5e-7f-ff-fa     statique
    255.255.255.255       ff-ff-ff-ff-ff-ff     statique

🌞 Manipuler la table ARP

    PS C:\Windows\system32> arp -d
    PS C:\Windows\system32> arp /a

    Interface : 169.254.148.110 --- 0x7
    Adresse Internet      Adresse physique      Type
    224.0.0.22            01-00-5e-00-00-16     statique

    Interface : 10.33.72.140 --- 0x9
    Adresse Internet      Adresse physique      Type
    10.33.71.24           cc-d9-ac-c4-17-4c     dynamique
    10.33.79.254          7c-5a-1c-d3-d8-76     dynamique
    224.0.0.22            01-00-5e-00-00-16     statique

    Interface : 192.168.1.2 --- 0xb
    Adresse Internet      Adresse physique      Type
    224.0.0.22            01-00-5e-00-00-16     statique

    Interface : 169.254.87.5 --- 0x12
    Adresse Internet      Adresse physique      Type
    224.0.0.22            01-00-5e-00-00-16     statique

    Interface : 169.254.236.202 --- 0x15
    Adresse Internet      Adresse physique      Type
    224.0.0.22            01-00-5e-00-00-16     statique
    PS C:\Windows\system32> ping 192.168.1.1

    Envoi d’une requête 'Ping'  192.168.1.1 avec 32 octets de données :
    Réponse de 192.168.1.1 : octets=32 temps=6 ms TTL=64
    Réponse de 192.168.1.1 : octets=32 temps=3 ms TTL=64
    Réponse de 192.168.1.1 : octets=32 temps=4 ms TTL=64
    Réponse de 192.168.1.1 : octets=32 temps=4 ms TTL=64

    Statistiques Ping pour 192.168.1.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
    Durée approximative des boucles en millisecondes :
    Minimum = 3ms, Maximum = 6ms, Moyenne = 4ms
    PS C:\Windows\system32> arp /a

    Interface : 169.254.148.110 --- 0x7
    Adresse Internet      Adresse physique      Type
    224.0.0.22            01-00-5e-00-00-16     statique
    224.0.0.251           01-00-5e-00-00-fb     statique
    239.255.255.250       01-00-5e-7f-ff-fa     statique
    255.255.255.255       ff-ff-ff-ff-ff-ff     statique

    Interface : 10.33.72.140 --- 0x9
    Adresse Internet      Adresse physique      Type
    10.33.65.136          74-3a-f4-ce-4d-61     dynamique
    10.33.71.24           cc-d9-ac-c4-17-4c     dynamique
    10.33.76.133          3c-9c-0f-c8-77-ec     dynamique
    10.33.79.254          7c-5a-1c-d3-d8-76     dynamique
    224.0.0.22            01-00-5e-00-00-16     statique
    224.0.0.251           01-00-5e-00-00-fb     statique
    239.255.255.250       01-00-5e-7f-ff-fa     statique

    Interface : 192.168.1.2 --- 0xb
    Adresse Internet      Adresse physique      Type
    192.168.1.1           00-d8-61-8a-6e-78     dynamique
    224.0.0.22            01-00-5e-00-00-16     statique
    224.0.0.251           01-00-5e-00-00-fb     statique
    239.255.255.250       01-00-5e-7f-ff-fa     statique

    Interface : 169.254.87.5 --- 0x12
    Adresse Internet      Adresse physique      Type
    224.0.0.22            01-00-5e-00-00-16     statique
    224.0.0.251           01-00-5e-00-00-fb     statique
    239.255.255.250       01-00-5e-7f-ff-fa     statique

    Interface : 169.254.236.202 --- 0x15
    Adresse Internet      Adresse physique      Type
    224.0.0.22            01-00-5e-00-00-16     statique
    224.0.0.251           01-00-5e-00-00-fb     statique
    239.255.255.250       01-00-5e-7f-ff-fa     statique

🌞 Wireshark it

## III. DHCP

🌞 Wireshark it