# I. ARP

## 1. Echange ARP

🌞Générer des requêtes ARP

    John :

    [yanice@localhost ~]$ ping 10.3.1.12
    PING 10.3.1.12 (10.3.1.12) 56(84) bytes of data.
    64 bytes from 10.3.1.12: icmp_seq=1 ttl=64 time=1.91 ms
    64 bytes from 10.3.1.12: icmp_seq=2 ttl=64 time=0.503 ms
    64 bytes from 10.3.1.12: icmp_seq=3 ttl=64 time=2.03 ms
    ^C
    --- 10.3.1.12 ping statistics ---
    3 packets transmitted, 3 received, 0% packet loss, time 2046ms
    rtt min/avg/max/mdev = 0.503/1.482/2.030/0.693 ms
#
    Marcel :

    [yanice@localhost ~]$ ping 10.3.1.11
    PING 10.3.1.11 (10.3.1.11) 56(84) bytes of data.
    64 bytes from 10.3.1.11: icmp_seq=1 ttl=64 time=1.62 ms
    64 bytes from 10.3.1.11: icmp_seq=2 ttl=64 time=0.699 ms
    64 bytes from 10.3.1.11: icmp_seq=3 ttl=64 time=1.51 ms
    ^C
    --- 10.3.1.11 ping statistics ---
    3 packets transmitted, 3 received, 0% packet loss, time 2004ms
    rtt min/avg/max/mdev = 0.699/1.276/1.623/0.411 ms
#
    John :

    [yanice@localhost ~]$ ip neigh show
    10.3.1.12 dev enp0s3 lladdr 08:00:27:f9:da:19 🌞 STALE
    10.3.1.1 dev enp0s3 lladdr 0a:00:27:00:00:21 REACHABLE
#
    Marcel :

    [yanice@localhost ~]$ ip neigh show
    10.3.1.11 dev enp0s3 lladdr 08:00:27:47:53:8d 🌞 STALE
    10.3.1.1 dev enp0s3 lladdr 0a:00:27:00:00:21 REACHABLE
#
    John :

    [yanice@localhost ~]$ ip neigh show 10.3.1.12
    10.3.1.12 dev enp0s3 lladdr 08:00:27:f9:da:19 STALE
#
    Mercel : 

    [yanice@localhost ~]$ ip a
    1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
    2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:f9:da:19 🌞 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.12/24 brd 10.3.1.255 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fef9:da19/64 scope link
       valid_lft forever preferred_lft forever

## 2. Analyse de trames

🌞Analyse de trames

    John : 

    [yanice@localhost ~]$ sudo tcpdump
    dropped privs to tcpdump
    tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
    listening on enp0s3, link-type EN10MB (Ethernet), snapshot length 262144 bytes
    15:47:41.590812 IP localhost.localdomain.ssh > 10.3.1.1.59028: Flags [P.], seq 360182381:360182441, ack 626795378, win 501, length 60
    15:47:41.592653 IP localhost.localdomain.ssh > 10.3.1.1.59028: Flags [P.], seq 60:96, ack 1, win 501, length 36
    15:47:41.593502 IP 10.3.1.1.59028 > localhost.localdomain.ssh: Flags [.], ack 96, win 8193, length 0
    15:47:41.594449 IP localhost.localdomain.ssh > 10.3.1.1.59028: Flags [P.], seq 96:204, ack 1, win 501, length 108
    15:47:41.595247 IP localhost.localdomain.ssh > 10.3.1.1.59028: Flags [P.], seq 204:240, ack 1, win 501, length 36
    15:47:41.595926 IP 10.3.1.1.59028 > localhost.localdomain.ssh: Flags [.], ack 240, win 8193, length 0
    15:47:41.647209 IP localhost.localdomain.ssh > 10.3.1.1.59028: 

## II. Routage

### 1. Mise en place du routage

🌞Ajouter les routes statiques nécessaires pour que john et marcel puissent se ping
